#include "mem.h"

#define PARTITION_SIZE 8192

void debug(const char *fmt, ...);

static void test_allocating() {
  debug("Started allocating test\n");

  void *heap = heap_init(0);

  void *partition_a = _malloc(8);
  void *partition_b = _malloc(sizeof(size_t));
  void *partition_c = _malloc(8192);

  debug_heap(stdout, heap);

  _free(partition_a);
  _free(partition_b);
  _free(partition_c);

  debug_heap(stdout, heap);

  heap_term();

  debug("Successfully\n");
}

static void test_dooming() {
  debug("Started allocating test\n");

  void *heap = heap_init(0);

  void *partition_a = _malloc(8192);
  void *partition_b = _malloc(8192);
  void *partition_c = _malloc(8192);

  debug_heap(stdout, heap);

  _free(partition_b);
  void *partition_d = _malloc(8192);
  _free(partition_a);
  void *partition_e = _malloc(8192);
  void *partition_f = _malloc(8192);
  _free(partition_c);

  _free(partition_e);
  _free(partition_f);
  _free(partition_d);

  debug_heap(stdout, heap);

  heap_term();

  debug("Successfully\n");
}

int main() {
  test_allocating();
  test_dooming();
}
